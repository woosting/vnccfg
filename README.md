# vnccfg

**VNC ConFiGuration** files (and startup commands).

1. Clone this repo into you homedir and rename the downloaded directory `mv vnccfg .vnc`.

2. Issue the following to start VNC server from the CLI (for a 1920x1200 monitor):

```shell
vncserver -localhost no -geometry 1920x1170
```

or (for 4k):

```shell
vncserver -localhost no -geometry 3840x2130
```

- **-localhost no** Sets local-host connections only mode off (to be able to connect from other devices).
- **-geometry** Sets the resolution.